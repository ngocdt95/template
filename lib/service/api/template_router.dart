import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'api_client.dart';
import 'base_model.dart';

enum TemplateEndPoint { getHome }

class TemplateRouter extends BaseRouter {
  TemplateEndPoint endPoint;
  dynamic data;
  String id;
  bool isLoading = true;
  CancelToken cancelToken;
  BuildContext context;
  TemplateRouter(this.context, this.endPoint, this.data,
      {this.id, this.isLoading, this.cancelToken});

  @override
  Future<BaseModel> get call async {
    Future<Response<dynamic>> response;
    switch (this.endPoint) {
      case TemplateEndPoint.getHome:
        response = client(headers: headerParams)
            .get(path, queryParameters: data, cancelToken: cancelToken);
        break;
    }

    return await BaseModel.onBaseModel(context, response,
        isShowLoading: isShowLoading());
  }

  @override
  Map<String, dynamic> get headerParams {
    Map<String, dynamic> headers = Map<String, dynamic>();
    return headers;
  }

  @override
  bool isShowLoading() {
    return this.isLoading;
  }

  @override
  String get path {
    var path = "";
    switch (this.endPoint) {
      case TemplateEndPoint.getHome:
        path = 'home';
        break;
    }
    return path;
  }
}
