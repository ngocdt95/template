import 'package:dio/dio.dart';
import 'package:template/application/configuration.dart';

import 'base_model.dart';

Dio client({Map<String, dynamic> headers}) {
  var dio = Dio();
  dio.options.connectTimeout = 60000;
  // dio.options.baseUrl = "http://172.168.192.211:1337/"; //
  dio.options.baseUrl = Configuration.shared.getValue("baseURL") + "/";
  if (headers != null) {
    dio.options.headers = headers;
  }
  _setupLoggingInterceptor(dio);
  return dio;
}

_setupLoggingInterceptor(Dio dio) {
  dio.interceptors.add(InterceptorsWrapper(onRequest: (RequestOptions options) {
    print("--> ${options.method} ${options.path} ${options.headers}");
    print("Content type: ${options.contentType}");
    print("Data: ${options.data}");
    return options; //continue
  }, onResponse: (Response response) {
    // Do something with response data
    print(
        "<-- ${response.statusCode} ${response.request.method} ${response.request.path}");
    String responseAsString = response.data.toString();
    print(responseAsString);
    print("<-- END HTTP -->");
    return response; // continue
  }, onError: (DioError e) {
    // Do something with response error
    return e; //continue
  }));
}

abstract class BaseRouter {
  String get path;
  Future<BaseModel> get call;
  Map<String, dynamic> get headerParams;
  bool isShowLoading();
}
