import 'dart:js';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'api_client.dart';
import 'base_model.dart';

enum HomeEndPoint { home }

class HomeRouter extends BaseRouter {
  HomeEndPoint endPoint;
  dynamic data;
  String id;
  bool isLoading = false;
  CancelToken cancelToken;
  BuildContext context;
  HomeRouter(this.context, this.endPoint, this.data,
      {this.id, this.isLoading, this.cancelToken});

  @override
  Future<BaseModel> get call async {
    Future<Response<dynamic>> response;
    switch (this.endPoint) {
      case HomeEndPoint.home:
        response = client(headers: headerParams)
            .get(path, queryParameters: data, cancelToken: cancelToken);
        break;
    }

    return await BaseModel.onBaseModel(context, response,
        isShowLoading: isShowLoading());
  }

  @override
  Map<String, dynamic> get headerParams {
    Map<String, dynamic> headers = Map<String, dynamic>();
    return headers;
  }

  @override
  bool isShowLoading() {
    return false;
  }

  @override
  String get path {
    var path = "";
    switch (this.endPoint) {
      case HomeEndPoint.home:
        path = 'home';
        break;
    }
    return path;
  }
}
