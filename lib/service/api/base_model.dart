import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:template/application/constant.dart';
import 'package:template/application/global.dart';
import 'package:template/manager/share_preference/share_preference.dart';
import 'package:template/service/navigation/navigation.dart';

class BaseModel {
  List<Map<String, dynamic>> dataList;

  Map<String, dynamic> data;

  BaseModel({this.dataList, this.data});

  BaseModel.fromJson(Map<String, dynamic> json) {
    data = json;
  }
  BaseModel.fromJsonList(List<Map<String, dynamic>> json) {
    dataList = json;
  }

  static onBaseModel(BuildContext context, Future<Response<dynamic>> response,
      {bool isShowLoading = false}) async {
    DioError err;
    var result = await response.catchError((onError) {
      err = (onError as DioError);
    });

    var model;
    if (result != null) {
      if (result.data is List) {
        model = BaseModel.fromJsonList(result.data);
      } else {
        model = BaseModel.fromJson(result.data);
      }
      return model;
    } else {
      try {
        if (err.response.statusCode == 401) {
          Navigation.pushReplaceTo(LoginRoute);
          Storage.clear();
        } else if (err.response.statusCode == 500 ||
            err.response.statusCode == 502) {
          showMessage(context, "Lỗi hệ thống");
        } else {
          var data = err.response.data as Map<String, dynamic>;
          showMessage(context, data["message"]);
        }
      } catch (e) {
        if (err.response.statusCode == 401) {
          Navigation.pushReplaceTo(LoginRoute);
          Storage.clear();
        } else {
          var data = err.response.data;
          showMessage(context, data.toString());
        }
      }
    }
  }
}
