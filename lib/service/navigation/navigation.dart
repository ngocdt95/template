import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Navigation {
  static GlobalKey<NavigatorState> key = GlobalKey<NavigatorState>();
  static pushTo(String routeName, {Object arguments}) async {
    return await key.currentState.pushNamed(routeName, arguments: arguments);
  }

  static pushReplaceTo(String routeName, {Object arguments}) async {
    return await key.currentState.pushNamedAndRemoveUntil(
        routeName, (Route<dynamic> route) => false,
        arguments: arguments);
  }

  static pop({dynamic result}) async {
    return key.currentState.pop(result);
  }
}
