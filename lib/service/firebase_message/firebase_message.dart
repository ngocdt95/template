import 'package:firebase_messaging/firebase_messaging.dart';

class FirebaseManger {
  static FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  static getInstance(
      Function(String tag, Map<String, dynamic> message) onMessage,
      Function(String token) onToken) {
    _firebaseMessaging.configure(
      onMessage: (data) => onMessage("onMessage", data),
      onLaunch: (data) => onMessage("onLaunch", data),
      onResume: (data) => onMessage("onResume", data),
    );
    _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(
            sound: true, badge: true, alert: true, provisional: true));

    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
    _firebaseMessaging.getToken().then((String token) {
      assert(token != null);
      print('FirebaseToken : $token');
      onToken(token);
    });
  }
}
