import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:template/application/configuration.dart';
import 'package:template/application/enum.dart';
import 'package:template/manager/share_preference/share_preference.dart';
import 'package:template/repository/auth_repository.dart';
import 'package:template/repository/template_repository.dart';
import 'package:template/view_model/app_view_model.dart';

GetIt getIt = GetIt.instance;

class Locator {
  static init() {
    WidgetsFlutterBinding.ensureInitialized();
    getIt.registerSingleton(Configuration(environment: Env.development));
    getIt.registerSingleton(AppViewModel());
    getIt.registerSingletonAsync(() async {
      await Storage.init;
    });
    getIt.registerLazySingleton(() => TemplateRepository());
    getIt.registerLazySingleton(() => AuthRepository());
  }
}
