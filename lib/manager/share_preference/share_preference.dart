import 'package:shared_preferences/shared_preferences.dart';

class Storage {
  static SharedPreferences _preferences;
  static get init async {
    print("SharedPreferences instance");
    _preferences = await SharedPreferences.getInstance();
  }

  static dynamic getValue(String key) => _preferences.get(key);
  static setBool(String key, dynamic value) => _preferences.setBool(key, value);
  static setString(String key, dynamic value) =>
      _preferences.setString(key, value);
  static setInt(String key, dynamic value) => _preferences.setInt(key, value);
  static setDouble(String key, dynamic value) =>
      _preferences.setDouble(key, value);
  static setStringList(String key, dynamic value) =>
      _preferences.setStringList(key, value);
  static clear() {
    if (_preferences != null) _preferences.clear();
  }
}
