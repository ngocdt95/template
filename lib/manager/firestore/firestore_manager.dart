import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';

///Thêm enum đại diện cho tên collection ở đây
class CollectionPath {
  static const conversation = "conversation";
  static const message = "message";
}

class FSManager {
  static FirebaseFirestore get fireStore => _firestore;
  static FirebaseFirestore _firestore;
  static Future<FSManager> get instance async {
    print("FSManager initialized");
    await Firebase.initializeApp();
    _firestore = FirebaseFirestore.instance;
    return FSManager();
  }
}

CollectionReference collection(String name) =>
    FSManager.fireStore.collection(name);

DocumentReference document(String name, String path) =>
    collection(name).doc(path);

DocumentReference documentBy(CollectionReference collection, String path) =>
    collection.doc(path);

CollectionReference collectionBy(DocumentReference document, String name) =>
    document.collection(name);

Stream<QuerySnapshot> streamDataCollection(String name) =>
    collection(name).snapshots();

Stream<QuerySnapshot> streamBy(CollectionReference collection) =>
    collection.snapshots();

Future<QuerySnapshot> dataCollection(String name) => collection(name).get();

Future<DocumentReference> addDocument(String name, Map data) =>
    collection(name).add(data);

Future<void> updateDocument(String name, String path, Map data) =>
    collection(name).doc(path).update(data);
