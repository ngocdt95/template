import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:uuid/uuid.dart';

import 'firestore_manager.dart';

class Template {
  int value = 1;
  int page = 1;
  List<Map<String, dynamic>> messageList = [];
  final conversationId = "1"; //Uuid().v4();

  Template() {
    //Lắng nghe sự kiện bao gồm dữ liệu thêm, sửa ,xóa
    streamBy(collectionMessage()).listen((event) {
      final docChanges = event?.docChanges?.length ?? 0;
      if (docChanges != 0) {
        final change = event?.docChanges?.last;
        if (change.type == DocumentChangeType.added) {
          messageList.add(event.docs.last.data());
        } else if (event.docChanges.last.type == DocumentChangeType.modified) {
        } else {
          event.docs.forEach((element) {
            int index = messageList
                .indexWhere((data) => data["id"] == element.data()["id"]);
            if (index == 0) {
              messageList.remove(index);
            }
          });
        }
      }
    });
  }

  //Mở kết nối đến một collection collection/:id/collection
  CollectionReference collectionMessage() {
    final doc = document(CollectionPath.conversation, conversationId);
    return collectionBy(doc, CollectionPath.message);
  }

  getData() async {
    final data = await getMessageList();
    messageList = data.docs.map((e) => e.data()).toList();
  }

  //Get message and loadmore
  Future<QuerySnapshot> getMessageList() async {
    return await collectionMessage()
        .orderBy("date", descending: false)
        .limit(page * 40)
        .get();
  }

  //Gửi dữ liệu
  sendData() async {
    var uuid = Uuid();
    final messageId = uuid.v1();
    value++;
    documentBy(collectionMessage(), messageId).set(
        {"data": value, "date": DateTime.now().toString(), "id": messageId});
  }
}
