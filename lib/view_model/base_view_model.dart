import 'package:flutter/material.dart';

enum ViewState {
  idle,
  loading,
  done,
  error,
  empty
}

class BaseViewModel extends ChangeNotifier {
  ViewState viewState;
  viewWithState(ViewState state) {
    viewState = state;
    notifyListeners();
  }

  notify(){
    notifyListeners();
  }
}