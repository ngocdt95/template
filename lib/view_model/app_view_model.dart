// import 'package:firebase_messaging/firebase_messaging.dart';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:template/service/firebase_message/firebase_message.dart';
import 'package:template/service/local_notification/local_notications_helper.dart';

class AppViewModel extends ChangeNotifier {
  String title;
  String body;

  AppViewModel() {
    _initNotifiCation();
    FirebaseManger.getInstance((tag, message) {
      print("$tag");
      _backgroundMessageHandler(message);
    }, (token) {});
  }

  _initNotifiCation() async {
    final settingsAndroid =
        AndroidInitializationSettings('@drawable/notification');
    final settingsIOS = IOSInitializationSettings(
        onDidReceiveLocalNotification: (id, title, body, payload) async {
      print("onDidReceiveLocalNotification:" + payload);
    });
    await notifications.initialize(
      InitializationSettings(settingsAndroid, settingsIOS),
    );
  }

  static Future<dynamic> _backgroundMessageHandler(
      Map<String, dynamic> message) async {
    print("onMessage: $message");
    Map<String, dynamic> data;
    String title = "";
    String body = "";
    if (Platform.isIOS) {
      title = message['aps']['alert']['title'];
      body = message['aps']['alert']['body'];
      data = message;
    } else {
      title = message["notification"]["title"];
      body = message["notification"]["body"];
      data = message;
    }

    await showOngoingNotification(notifications, title: title, body: body);
  }
}
