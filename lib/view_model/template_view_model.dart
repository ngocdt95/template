import 'package:flutter/cupertino.dart';
import 'package:template/locator.dart';
import 'package:template/model/template_model.dart';
import 'package:template/repository/template_repository.dart';
import 'package:template/service/api/template_router.dart';
import 'package:template/application/extension.dart';
import 'package:template/view_model/base_view_model.dart';

class TemplateViewModel extends BaseViewModel {
  TemplateRepository authRepository = getIt<TemplateRepository>();
  int count = 0;
  BuildContext context;
  TemplateModel templateModel;

  TemplateViewModel(this.context);
  onReadyModel() {
    getHomeList();
  }

  add() {
    count++;
    notifyListeners();
  }

  sub() {
    count--;
    notifyListeners();
  }

  getHomeList() async {
    viewWithState(ViewState.loading);
    try {
      var result = await TemplateRouter(context, TemplateEndPoint.getHome, null,
              isLoading: true)
          .call;
      if (result?.data != null) {
        templateModel = TemplateModel.fromJson(result.data);
        viewWithState(ViewState.done);
      } else {
        viewWithState(ViewState.empty);
      }
    } catch (e) {
      viewWithState(ViewState.error);
    }
  }
}
