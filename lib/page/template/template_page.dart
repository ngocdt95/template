import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'package:template/application/configuration.dart';
import 'package:template/view_model/base_view_model.dart';
import 'package:template/view_model/template_view_model.dart';

class TemplatePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<TemplateViewModel>.reactive(
      builder: (c, viewModel, widget) {
        return Scaffold(
          body: Container(
            child: Builder(builder: (c) {
              if (viewModel.viewState == ViewState.loading) {
                return Center(child: CupertinoActivityIndicator());
              } else if (viewModel.viewState == ViewState.error) {
                return Center(child: Text("Error"));
              } else {
                return ListView(
                  children: [
                    Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: 16, vertical: 16),
                            child: SizedBox(
                              child: Text(
                                "Dịch vụ thú cưng",
                                style: TextStyle(
                                    fontSize: 18, fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                          Container(
                            height: 90,
                            child: ListView(
                              padding: EdgeInsets.symmetric(horizontal: 16),
                              scrollDirection: Axis.horizontal,
                              children: viewModel.templateModel == null
                                  ? []
                                  : viewModel.templateModel.events
                                      .map((item) => Padding(
                                          padding: EdgeInsets.only(
                                              left: viewModel
                                                          .templateModel.events
                                                          .indexOf(item) ==
                                                      0
                                                  ? 0
                                                  : 16),
                                          child: InkWell(
                                            onTap: () {},
                                            child: FadeInImage.assetNetwork(
                                                placeholder:
                                                    "assets/images/placeholder.png",
                                                height: 90,
                                                width: 150,
                                                image:
                                                    "${Configuration.shared.getValue("baseURL")}${item?.thumbnail?.url ?? ""}"),
                                          )))
                                      .toList(),
                            ),
                          ),
                          GridView(
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            gridDelegate:
                                SliverGridDelegateWithFixedCrossAxisCount(
                                    crossAxisCount: 4,
                                    childAspectRatio: 0.85,
                                    mainAxisSpacing: 10,
                                    crossAxisSpacing: 20),
                            children: viewModel.templateModel == null
                                ? []
                                : viewModel.templateModel.services.map((item) {
                                    return InkWell(
                                      onTap: () {},
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.stretch,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceEvenly,
                                        children: <Widget>[
                                          FadeInImage.assetNetwork(
                                              placeholder:
                                                  "assets/images/placeholder.png",
                                              height: 55,
                                              image:
                                                  "${Configuration.shared.getValue("baseURL")}${item?.thumbnail?.url ?? ""}"),
                                          Text(
                                            item?.name ?? "",
                                            overflow: TextOverflow.ellipsis,
                                            textAlign: TextAlign.center,
                                          )
                                        ],
                                      ),
                                    );
                                  }).toList(),
                          )
                        ],
                      ),
                    ),
                  ],
                );
              }
            }),
          ),
          floatingActionButton: FloatingActionButton(onPressed: () {
            viewModel.getHomeList();
          }),
        );
      },
      viewModelBuilder: () => TemplateViewModel(context),
      onModelReady: (m) => m.onReadyModel(),
    );
  }
}
