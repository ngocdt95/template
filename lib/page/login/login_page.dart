import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'package:template/manager/share_preference/share_preference.dart';
import 'package:template/view_model/login_view_model.dart';

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<LoginViewModel>.reactive(
        builder: (context, viewModel, widget) {
          return Scaffold(
            body: Center(
              child: Text("Login"),
            ),
            floatingActionButton: FloatingActionButton(onPressed: () {
              Storage.clear();
            }),
          );
        },
        viewModelBuilder: () => LoginViewModel());
  }
}
