import 'package:flutter/material.dart';
import 'package:template/application/constant.dart';
import 'package:template/application/extension.dart';
import 'package:template/manager/share_preference/share_preference.dart';

class SplashPage extends StatefulWidget {
  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(seconds: 2)).whenComplete(() {
      // context.pushTo(
      //     Storage.getValue("isLogin") == true ? LoginRoute : TemplateRoute);
      context.pushTo(TemplateRoute);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text("Splash"),
      ),
    );
  }
}
