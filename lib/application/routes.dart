import 'package:flutter/material.dart';
import 'package:template/page/login/login_page.dart';
import 'package:template/page/splash/splash_page.dart';
import 'package:template/page/template/template_page.dart';

import 'constant.dart';

class RoutingData {
  final String route;
  final Map<String, String> _queryParameters;

  RoutingData({
    this.route,
    Map<String, String> queryParameters,
  }) : _queryParameters = queryParameters;

  operator [](String key) => _queryParameters[key];
}

extension StringExtension on String {
  RoutingData get getRoutingData {
    var uriData = Uri.parse(this);
    print('queryParameters: ${uriData.queryParameters} path: ${uriData.path}');
    return RoutingData(
      queryParameters: uriData.queryParameters,
      route: uriData.path,
    );
  }
}

Route<dynamic> generateRoute(RouteSettings settings) {
  var routingData = settings.name.getRoutingData; // Get the routing Data
  PageRoute<dynamic> pageRoute;
  switch (routingData.route) {
    case RootRoute:
      pageRoute = _getPageRoute(SplashPage(), settings);
      break;
    case LoginRoute:
      pageRoute = _getPageRoute(LoginPage(), settings);
      break;
    case TemplateRoute:
      pageRoute = _getPageRoute(TemplatePage(), settings);
      break;
  }
  return pageRoute;
}

PageRoute _getPageRoute(Widget child, RouteSettings settings,
    {bool fullscreenDialog = false}) {
  return MaterialPageRoute(
      builder: (c) => child,
      settings: settings,
      fullscreenDialog:
          fullscreenDialog); //_FadeRoute(child: child, settings: settings);
}

PageRoute _getFadePageRoute(Widget child, RouteSettings settings,
    {bool fullscreenDialog = false}) {
  return _FadeRoute(child: child, settings: settings);
}

class _FadeRoute extends PageRouteBuilder {
  final Widget child;
  final RouteSettings settings;

  _FadeRoute({this.child, this.settings})
      : super(
            pageBuilder: (
              BuildContext context,
              Animation<double> animation,
              Animation<double> secondaryAnimation,
            ) =>
                child,
            settings: settings,
            transitionsBuilder: (
              BuildContext context,
              Animation<double> animation,
              Animation<double> secondaryAnimation,
              Widget child,
            ) =>
                FadeTransition(
                  opacity: animation,
                  child: child,
                ));
}
