import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:template/service/navigation/navigation.dart';

extension NavigationBuildContext on BuildContext {
  pushTo(String routeName, {Object arguments}) =>
      Navigation.pushTo(routeName, arguments: arguments);
  pushReplaceTo(String routeName, {Object arguments}) =>
      Navigation.pushReplaceTo(routeName, arguments: arguments);
  pop({dynamic result}) => Navigation.pop(result: result);
  showLoading() {
    return showDialog(
        context: this,
        barrierDismissible: false,
        builder: (c) {
          return Scaffold(
            backgroundColor: Colors.transparent,
            body: Center(
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.white,
                ),
                alignment: Alignment.center,
                height: 70,
                width: 70,
                child: CupertinoActivityIndicator(),
              ),
            ),
          );
        });
  }

  hideLoading() => Navigation.pop();
}
