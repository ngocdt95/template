import 'package:template/application/constant.dart';
import 'package:template/application/enum.dart';
import 'package:template/locator.dart';

class Configuration {
  static Configuration shared = getIt<Configuration>();
  final Env environment;
  Map<String, dynamic> map;

  Configuration({this.environment = Env.development}) {
    switch (environment) {
      case Env.development:
        map = devConfig;
        break;
      case Env.staging:
        map = stagingConfig;
        break;
      case Env.production:
        map = productionConfig;
        break;
    }
  }
  getValue(String key) => map[key];
}
