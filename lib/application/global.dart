import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:template/manager/toast/toast.dart';

showMessage(BuildContext context, String message,
    {int gravity = 0, Color color}) {
  Toast.show(message, context, gravity: Toast.CENTER, duration: 4);
}

dismissKeyboard(BuildContext context) {
  SystemChannels.textInput.invokeMethod('TextInput.hide');
  FocusScope.of(context).unfocus();
}
