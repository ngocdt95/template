//Environment
const devConfig = {"baseURL": "http://okela.me:1337"};
const stagingConfig = {"baseURL": "https://jsonplaceholder.typicode.com/"};
const productionConfig = {"baseURL": "https://jsonplaceholder.typicode.com/"};

//Route
const RootRoute = "/";
const SplashRoute = "/splash";
const LoginRoute = "/login";
const TemplateRoute = "/template";
