import 'package:flutter/material.dart';
import 'package:template/application/constant.dart';
import 'package:template/application/routes.dart';
import 'package:template/locator.dart';
import 'package:template/manager/auto_size/auto_size.dart';

import 'service/navigation/navigation.dart';

void main() {
  Locator.init();
  runAutoSizeApp(MaterialApp(
    navigatorKey: Navigation.key,
    debugShowCheckedModeBanner: false,
    initialRoute: RootRoute,
    onGenerateRoute: generateRoute,
    theme: ThemeData(
      primarySwatch: Colors.blue,
      visualDensity: VisualDensity.adaptivePlatformDensity,
    ),
  ));
}
